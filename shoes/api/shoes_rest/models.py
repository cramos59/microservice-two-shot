from django.db import models

# Create your models here.

class BinVO(models.Model):
    closet_name = models.CharField(max_length=156)
    bin_number = models.PositiveSmallIntegerField(null=True, blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True, blank=True)
    import_href = models.CharField(max_length=200, null=True, blank=True, unique=True)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()
    bin = models.ForeignKey(
        BinVO, related_name="shoe",
        null=True, blank=False,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.model_name
