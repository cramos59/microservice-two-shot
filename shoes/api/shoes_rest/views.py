from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoe

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href"]



class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]

    encoders = {
        "bin": BinVOEncoder()
    }

class ShoesDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]

    encoders = {
        "bin": BinVOEncoder()
    }




@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            shoes,
            encoder=ShoesListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        print("Received Content:", content)
        try:
            href = content["bin"]
            print("Bin Href:", href)
            bin = BinVO.objects.get(import_href=href)
            print("Found Bin:", bin)
            content["bin"] = bin
        except BinVO.DoesNotExist:

            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        print("Creating Shoe with Content:", content)
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
                shoes,
                encoder=ShoesDetailEncoder,
                safe=False,
            )
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json



@require_http_methods(["GET", "DELETE", "PUT"])
def api_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoesDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response =  JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder = ShoesDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response= JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response
    else:  
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)

            props = ["manufacturer", "model_name", "color", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoesDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response =JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response
