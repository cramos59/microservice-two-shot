import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadShoes() {
  const shoesResponse = await fetch('http://localhost:8080/api/shoes/');


  if (shoesResponse.ok)  {
    const shoeData = await shoesResponse.json();

    root.render(
      <React.StrictMode>
        <App shoes={shoeData} />
      </React.StrictMode>
    );
  } else {
    console.error(shoesResponse);
  }
}

loadShoes()


async function loadHats() {
  const hatsResponse = await fetch('http://localhost:8090/api/hats/');


  if (hatsResponse.ok)  {
    const hatsData = await hatsResponse.json();

    root.render(
      <React.StrictMode>
        <App shoes={hatsData} />
      </React.StrictMode>
    );
  } else {
    console.error(hatsResponse);
  }
}

loadHats()
