import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const deleteShoe = async (id) => {
  await fetch(`http://localhost:8080/api/shoes/${id}/`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  window.location.reload();
}

function ShoeColumn(props) {
  return (
    <div className="col">
      {props.list.map((data) => {
        const shoe = data;
        return (
          <div key={shoe.id} className="card mb-3 shadow">
            <img src={shoe.picture_url} className="card-img-top" alt="Shoe" />
            <div className="card-body">
              <h5 className="card-title">{shoe.manufacturer} {shoe.model_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {shoe.bin.closet_name}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Color: {shoe.color}
              </h6>
            </div>
            <div className="card-footer">
              <button onClick={() => deleteShoe(shoe.id)} type="button">
                Delete
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

function ShoeList() {
  const [shoeColumns, setShoeColumns] = useState([[], [], []]);

  useEffect(() => {
    async function fetchShoeData() {
      const url = 'http://localhost:8080/api/shoes/';
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();

          const requests = data.map((shoe) =>
            fetch(`http://localhost:8080/api/shoes/${shoe.id}`)
          );

          const responses = await Promise.all(requests);
          let i = 0;
          const updatedShoeColumns = [[], [], []];
          for (const shoeResponse of responses) {
            if (shoeResponse.ok) {
              const details = await shoeResponse.json();
              updatedShoeColumns[i].push(details);
              i = (i + 1) % 3;
            } else {
              console.error(shoeResponse);
            }
          }
          setShoeColumns(updatedShoeColumns);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchShoeData();
  }, []);

  return (
    <>
      {/* Header Section */}
      {/* ... */}

      {/* Main Shoe Display */}
      <div className="container">
        <h2>Look at all them Shoes</h2>
        <div className="row">
          {shoeColumns.map((shoeList, index) => (
            <ShoeColumn key={index} list={shoeList} />
          ))}
        </div>
      </div>
    </>
  );
}

export default ShoeList;
