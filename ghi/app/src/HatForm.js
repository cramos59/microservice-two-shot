import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    };
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    };
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    };
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            manufacturer,
            model_name,
            color,
            picture_url,
            bin,
        };

        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoesResponse = await fetch(shoesUrl, fetchConfig);
        if (shoesResponse.ok) {
            const newShoe = await shoesResponse.json();
            console.log(newShoe);
            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }
    };

    const fetchData = async () => {
        const binsUrl = 'http://localhost:8080/api/bins/';

        const binsResponse = await fetch(binsUrl);
        if (binsResponse.ok) {
            const data = await binsResponse.json();
            setBins(data.bins);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleManufacturerChange}
                                value={manufacturer}
                                placeholder="Manufacturer"
                                required
                                type="text"
                                name="manufacturer"
                                id="manufacturer"
                                className="form-control"
                            />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleModelNameChange}
                                value={model_name}
                                placeholder="Model Name"
                                required
                                type="text"
                                name="model_name"
                                id="model_name"
                                className="form-control"
                            />
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleColorChange}
                                value={color}
                                placeholder="Color"
                                required
                                type="text"
                                name="color"
                                id="color"
                                className="form-control"
                            />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handlePictureUrlChange}
                                value={picture_url}
                                placeholder="Picture URL"
                                required
                                type="text"
                                name="picture_url"
                                id="picture_url"
                                className="form-control"
                            />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select
                                onChange={handleBinChange}
                                value={bin}
                                required
                                name="bin"
                                id="bin"
                                className="form-select"
                            >
                                <option value="">Choose a Bin</option>
                                {bins.map((bin) => {
                                    return (
                                        <option
                                            key={bin.id}
                                            value={bin.import_href}
                                        >
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;
