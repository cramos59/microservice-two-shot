from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json

from .models import Hats, LocationVO
from common.json import ModelEncoder
# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'import_href',
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatsListEnconder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "hat_color",
        "hat_url",
        "location",
    ]
    encoders = {
        'location' : LocationVODetailEncoder()
    }

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "hat_color",
        "hat_url",
        "location",
    ]
    encoders = {
        'location' : LocationVODetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def list_of_hats(request):
    if request.method == "GET":
            hats = Hats.objects.all()
            return JsonResponse(
                 hats,
                encoder=HatsListEnconder,
                safe=False
            )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            print(content)
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
            print(content)
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def detail_of_hats(request,pk):
    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=pk)
            return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
        except Hats.DoesNotExist:
            response =  JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hats.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder = HatsDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            response= JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            hat = Hats.objects.get(id=pk)

            props = ["fabric", "style_name", "hat_color", "hat_url"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatsDetailEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            response =JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response
